import app.App;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Platform;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Created by Larry on 11/10/2016.
 */
public class ClassChecker extends App {

    private static final int ESE306 = 41585;
    private static final int AMS301 = 40305;
    private static final int ESE300 = 41421;
    private static final int ESE346 = 40102;
    private static final int ESE211 = 40062;

    private static boolean enroll = false;
    private static String wavPath = "alarm.wav";
    private static boolean silentMode = false;
    private static boolean isPaused = false;
    private static boolean changePath = false;
    private static int addCourse = 0;
    private static CourseList myList;
    private Course newCourse;

    private static Thread checking;

    public static void main(String[] args) throws InterruptedException {

        ObjectMapper mapper = new ObjectMapper();
        try {
            myList = mapper.readValue(CourseList.class.getResource("myList.json"), CourseList.class);
            if(myList == null)
                myList = new CourseList();
        } catch (IOException e) {
            System.out.println("Previous list not found. New list created.");
            myList = new CourseList();
        }


        checking = new Thread(() -> {
            while(true)
            {
                try
                {
                    if(!isPaused)
                        check();
                }
                catch (Exception e){System.out.println("Error occurred! " + e.getMessage());}
                finally {
                    try {
                        Thread.sleep(300000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        launch(args);
    }

    private static void print(String message)
    {
        Platform.runLater(()-> console.appendText(message));
    }

    private static void println(String message)
    {
        System.out.println(message);
        Platform.runLater(()-> console.appendText(message + "\n"));
    }

    private static void clearConsole()
    {
        Platform.runLater(()-> console.clear());
    }

    private static void check() throws IOException {
        clearConsole();
        println("Current Time: " + LocalDateTime.now());
        println("---------------------------------------------------------------");


        for(int i = 0; i < myList.size(); i++)
            if(checkCourse(myList.get(i)))
                enroll = true;
        if(enroll)
        {
            println("GO!!!!!!!!!!!!!!!!!!!!!!!!");
            play(wavPath);
        }
        println("----------------------------------------------------------------");
    }

    private static boolean checkCourse(Course c) throws IOException {
        URL url = new URL("http://classfind.stonybrook.edu/vufind/AJAX/JSON?method=getItemVUStatuses&itemid="
                + c.getClassCode() + "&strm=1174");
        ObjectMapper mapper = new ObjectMapper(); // just need one
        // Got a Java class that data maps to nicely? If so:
        Map<String,String> map = mapper.readValue(url, Map.class);
        String text = map.get("data");
        String tag = "<SU_ENRL_AVAL>";
        String closeTag = "</SU_ENRL_AVAL>";
        String waitlistTag = "<WAITLIST_POS>";
        String closeWaitlistTag = "</WAITLIST_POS>";
        String aval = text.substring(text.indexOf(tag) + tag.length(), text.indexOf(closeTag));
        String waitlist = text.substring(text.indexOf(waitlistTag) + waitlistTag.length(), text.indexOf(closeWaitlistTag));
        println(c.toString());
        println("Available: " + aval);
        println("Waitlist Position: " + waitlist);
        println("");

        return Integer.parseInt(aval) + Integer.parseInt(waitlist) >= c.getAlarm();
    }

    private static void play(String path) throws IOException
    {
        if(silentMode)
            return;
        InputStream in = new FileInputStream(path);
        AudioStream audioStream = new AudioStream(in);
        AudioPlayer.player.start(audioStream);
    }

    private static void save()
    {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(CourseList.class.getResource("myList.json").getFile()), myList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void setup() {
        checking.start();
        input.setOnAction(event -> {
            if(changePath)
            {
                wavPath = input.getText();
                println("Alarm set at " + wavPath);
                changePath = false;
                input.clear();
                return;
            }

            if(addCourse != 0)
            {
                switch(addCourse)
                {
                    case 1:
                        newCourse = new Course(input.getText(), -1, "Spring2016");
                        println("Course Name Set -- " + input.getText());
                        println("Please enter the course code");
                        addCourse++;
                        break;
                    case 2:
                        newCourse.setClassCode(Integer.parseInt(input.getText().trim()));
                        println("Set Alarm when available?");
                        addCourse++;
                        break;
                    case 3:
                        newCourse.setAlarm(Integer.parseInt(input.getText().trim()));
                        myList.add(newCourse);
                        Collections.sort(myList);
                        save();
                        println("Course " + newCourse.toString() + " added!");
                        addCourse = 0;
                }
                input.clear();
                return;
            }

            switch(input.getText().toLowerCase())
            {
                case "r":
                    try {check();} catch (Exception e) {e.printStackTrace();}
                    break;
                case "p":
                    isPaused = !isPaused;
                    println(isPaused ? "*****Paused*****" : "*****Resumed*****");
                    break;
                case "s": silentMode = !silentMode;
                    println(silentMode ? "*****Silent Mode ON*****" : "*****Silent Mode OFF*****");
                    break;
                case "c": changePath = true;
                    println("Enter the path of the file");
                    break;
                case "a":
                    println("Enter the course name");
                    addCourse = 1;
            }
            input.clear();

        });

    }
}
