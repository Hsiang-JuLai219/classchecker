/**
 * Created by Larry on 11/17/2016.
 */
public class Course implements Comparable<Course>{
    private int classCode;
    private String subject;
    private int courseNumber;
    private String term;
    private int year;
    private int alarm;

    public Course(){}

    public Course(String subjectNumber, int classCode, String term)
    {
        subjectNumber = subjectNumber.trim().toUpperCase();
        int blank = subjectNumber.indexOf(" ");
        this.subject = subjectNumber.substring(0, blank == -1 ? 3 : blank);
        blank = subjectNumber.lastIndexOf(" ");
        courseNumber = Integer.parseInt(subjectNumber.substring(blank == -1 ? 3 : blank + 1));

        this.classCode = classCode;
        this.term = term;
        this.year = Integer.parseInt(term.substring(term.length()-4));
    }

    public int getClassCode() {return classCode;}

    public void setClassCode(int classCode) {this.classCode = classCode;}

    public String getSubject() {return subject;}

    public void setSubject(String subject) {this.subject = subject;}

    public int getCourseNumber() {return courseNumber;}

    public void setCourseNumber(int courseNumber) {this.courseNumber = courseNumber;}

    public String getTerm() {return term;}

    public void setTerm(String term) {this.term = term;}

    public void setAlarm(int n){alarm = n;}

    public int getAlarm() {return alarm;}

    @Override
    public int compareTo(Course c) {
        if(year != c.year)
            return (c.year - year) * 1000;
        if(!term.equals(c.term))
            return term.compareTo(c.term) * 100;
        if(!subject.equals(c.subject))
            return subject.compareTo(c.subject);
        return courseNumber - c.courseNumber;
    }

    @Override
    public String toString(){return subject + courseNumber;}
}
