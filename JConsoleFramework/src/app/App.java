package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public abstract class App extends Application {

    protected static TextArea console;
    protected static TextField input;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("gui.fxml"));
        initWindow(primaryStage);
        primaryStage.setScene(initScene(root));
        setup();
        primaryStage.show();
    }

    protected void initWindow(Stage primaryStage)
    {
        primaryStage.setTitle("My Application");
    }

    protected Scene initScene(Parent root)
    {
        console = (TextArea)root.lookup("#console");
        input = (TextField)root.lookup("#input");
        console.getStylesheets().add(getClass().getResource("console.css").toExternalForm());

        return new Scene(root, 700, 700);
    }

    protected abstract void setup();
}
